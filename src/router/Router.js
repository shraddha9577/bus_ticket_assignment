// @imports
import React from "react";
import { Routes, Route } from "react-router-dom";

// @components
import Dashboard from "../component/dashboard/Dashboard";
import Reservation from "../component/reservation/Reservation";

const Router = () => (
  <Routes>
    <Route exact path="/" element={<Dashboard />} />
    <Route path="/reservation" element={<Reservation />} />
  </Routes>
);

export default Router;
