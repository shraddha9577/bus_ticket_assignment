// @imports
import React from "react";
import { BrowserRouter } from "react-router-dom";
import { ToastContainer } from "react-toast";
// @components
import Navbar from "./common/navbar/Navbar";
import Router from "./router/Router";
// @css
import "./style.css";

const App = () => (
  <BrowserRouter>
    <Navbar />
    <Router />
    <ToastContainer delay={3000} position="bottom-right" />
  </BrowserRouter>
);

export default App;
