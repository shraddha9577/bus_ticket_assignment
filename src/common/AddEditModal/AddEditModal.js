// @imports
import React, { useState, useEffect } from "react";
import {
  Button,
  TextField,
  Dialog,
  DialogActions,
  DialogContent,
  DialogTitle,
} from "@mui/material";
import { toast } from "react-toast";

// @material icons
import AddIcon from "@mui/icons-material/Add";

const AddEditModal = ({
  isEdit,
  ticket,
  handleClickOpen,
  open,
  handleAddEditTicket,
}) => {
  const [fname, setFname] = useState("");
  const [lname, setLname] = useState("");
  const [email, setEmail] = useState("");
  const [formError, setFormError] = useState({
    fnameError: false,
    lnameError: false,
    emailError: false,
  });

  useEffect(() => {
    if(Object.keys(ticket).length > 0){
      setFname(ticket?.fname);
      setLname(ticket?.lname);
      setEmail(ticket?.email);
    }
  }, [isEdit, ticket]);

  const validate = () => {
    let formError = false;
    let errors = {};

    if (fname === "") {
      errors.fnameError = true;
      formError = true;
    }

    if (lname === "") {
      errors.lnameError = true;
      formError = true;
    }

    if (email === "") {
      errors.emailError = true;
      formError = true;
    }
    setFormError(errors);
    return formError;
  };

  const handleSubmitValue = () => {
    if (!validate()) {
      handleAddEditTicket(fname, lname, email);
      clearForm();
      handleClickOpen();
      if (isEdit) toast.success("Data updated successfully!");
      else toast.success("New entry added successfully!");
    }
  };

  const clearForm = () => {
    setFormError({
      fnameError: false,
      lnameError: false,
      emailError: false,
    });
    setFname("");
    setLname("");
    setEmail("");
  };

  const { fnameError, lnameError, emailError } = formError;
  return (
    <div>
      <Dialog open={open}>
        <DialogTitle
          sx={{ display: "flex", alignItems: "center", color: "darkmagenta" }}
        >
          <AddIcon />
          {isEdit ? "Update" : "Add"}Ticket
        </DialogTitle>
        <DialogContent>
          <TextField
            autoFocus
            margin="dense"
            id="fname"
            label="First Name"
            type="text"
            fullWidth
            variant="standard"
            error={fnameError}
            helperText={fnameError ? "First Name is Required." : ""}
            value={fname}
            onChange={(e) => setFname(e.target.value)}
          />
          <TextField
            margin="dense"
            id="lname"
            label="Last Name"
            type="text"
            fullWidth
            variant="standard"
            error={lnameError}
            helperText={lnameError ? "Last Name is Required." : ""}
            value={lname}
            onChange={(e) => setLname(e.target.value)}
          />
          <TextField
            margin="dense"
            id="email"
            label="Email Address"
            type="email"
            fullWidth
            variant="standard"
            error={emailError}
            helperText={emailError ? "Email is Required." : ""}
            value={email}
            onChange={(e) => setEmail(e.target.value)}
          />
        </DialogContent>
        <DialogActions>
          <Button
            onClick={() => {
              handleClickOpen();
              clearForm();
            }}
          >
            Cancel
          </Button>
          <Button onClick={handleSubmitValue}>
            {isEdit ? "Save Updates" : "Add New"}
          </Button>
        </DialogActions>
      </Dialog>
    </div>
  );
};

export default AddEditModal;
