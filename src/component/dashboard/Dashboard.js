// @imports
import React, { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
import { toast } from "react-toast";
// @components
import Table from "../../common/table/Table";
import AddEditModal from "../../common/AddEditModal/AddEditModal";

const headCells = [
  {
    id: "name",
    numeric: false,
    disablePadding: true,
    label: "Name",
  },
  {
    id: "email",
    numeric: false,
    disablePadding: false,
    label: "Email",
  },
  {
    id: "seat",
    numeric: true,
    disablePadding: false,
    label: "Seat Number",
  },
  {
    id: "date",
    numeric: true,
    disablePadding: false,
    label: "Date Of Booking",
  },
  {
    id: "action",
    disablePadding: false,
    label: "Actions",
  },
];

const Dashboard = () => {
  const [passengerList, setPassengerList] = useState([]);
  const [open, setOpen] = React.useState(false);
  const [ticket, setTicket] = React.useState({});

  const handleClickOpen = () => setOpen(!true);

  const getReservationData = () => {
    const reservationList =
      JSON.parse(localStorage.getItem("reservation")) || [];
    setPassengerList(reservationList);
  };

  const handleDelete = (ids) => {
    const newList = passengerList.filter((item) => !ids.includes(item.seat));
    localStorage.setItem("reservation", JSON.stringify(newList));
    setPassengerList(newList);
    toast.success("Ticket data deleted successfully!");
  };

  const setTicketData = ({ fname, lname, email, seat }) => {
    setTicket({ fname, lname, email, seat });
    setOpen(true);
  };

  const handleUpdateTicket = (fname, lname, email) => {
    const reservationList =
      JSON.parse(localStorage.getItem("reservation")) || [];
    const ticketIndex = reservationList.findIndex((data) => data.seat === ticket.seat);

    if (ticketIndex !== -1) {
      const updatedReservationList = [...reservationList];
      const ticketData = { ...updatedReservationList[ticketIndex] };
      ticketData.fname = fname;
      ticketData.lname = lname;
      ticketData.email = email;
      ticketData.date = new Date();
      updatedReservationList[ticketIndex] = ticketData;

      localStorage.setItem(
        "reservation",
        JSON.stringify(updatedReservationList)
      );
    } else {
      toast.success("No ticket data found!");
    }
    getReservationData();
    setTicket({});
  };

  useEffect(() => {
    getReservationData();
  }, []);

  return (
    <>
      <Typography
        variant="h4"
        mt={3}
        sx={{ marginLeft: "140px", color: "darkmagenta" }}
      >
        Dashboard
      </Typography>
      <Typography variant="p" mt={3} sx={{ marginLeft: "140px" }}>
        Total <b>{40 - passengerList.length}</b> seat remaning out of 40.
      </Typography>
      <Grid container spacing={2} sx={{ justifyContent: "center" }}>
        <Grid item lg={10} md={10} mt={5}>
          <Table
            headCells={headCells}
            rows={passengerList}
            handleDelete={handleDelete}
            setTicketData={setTicketData}
          />
        </Grid>
      </Grid>

      {/* Edit Modal */}
      {open && Object.keys(ticket).length > 0 && (
        <AddEditModal
          isEdit={true}
          open={open}
          handleClickOpen={handleClickOpen}
          ticket={ticket}
          handleAddEditTicket={handleUpdateTicket}
        />
      )}
    </>
  );
};

export default Dashboard;
