// @imports
import React, { useEffect, useState } from "react";
import { Grid, Typography } from "@mui/material";
// @components
import AddEditModal from "../../common/AddEditModal/AddEditModal";

const BusLayout = () => {
  const [open, setOpen] = useState(false);
  const [seatNumber, setSeatNumber] = useState("");
  const [reservations, setReservation] = useState([]);

  useEffect(() => {
    setSeatNumber("");
    getReservationData();
  }, []);

  const getReservationData = () => {
    const reservationList =
      JSON.parse(localStorage.getItem("reservation")) || [];
    setReservation(reservationList);
  };

  const handleClickOpen = () => {
    setOpen(!true);
    setSeatNumber("");
  };

  const reserveTicket = (seat) => {
    setOpen(true);
    setSeatNumber(seat);
  };

  const handleAddTicket = (fname, lname, email) => {
    const packet = {
      fname,
      lname,
      email,
      seat: seatNumber,
      date: new Date(),
    };

    const newReservation = [...reservations, packet];
    localStorage.setItem("reservation", JSON.stringify(newReservation));
    getReservationData();
    setSeatNumber("");
  };

  const displaySeats = (number) => {
    const isBooked = reservations.find((ticket) => ticket?.seat == number);

    return isBooked ? (
      <Grid item key={number} className={`seat-box lower-desk booked`}>
        {number}
      </Grid>
    ) : (
      <Grid
        item
        key={number}
        className={`seat-box lower-desk ${
          seatNumber == number ? "selcted" : "available"
        }`}
        onClick={() => reserveTicket(number)}
      >
        {number}
      </Grid>
    );
  };

  return (
    <>
      <Typography variant="h6" mt={3} sx={{ color: "darkmagenta" }}>
        Choose Seat
      </Typography>
      <div className="seat-otions">
        <div className="seat-otions-item">
          <span className="available-option"></span>Available
        </div>
        <div className="seat-otions-item">
          <span className="booked-option"></span>Booked
        </div>
        <div className="seat-otions-item">
          <span className="selected-option"></span>Selected
        </div>
      </div>
      <Typography variant="h6" mt={3}>
        Lower Desk
      </Typography>
      <Grid container direction={"row"} className="lower-desk">
        {Array.from({ length: 20 }, (_, index) => index + 1).map((number) =>
          displaySeats(number)
        )}
      </Grid>

      <Typography variant="h6" mt={3}>
        Upper Desk
      </Typography>
      <Grid
        container
        direction={"row"}
        className="lower-desk"
        sx={{ marginBottom: "30px" }}
      >
        {Array.from({ length: 20 }, (_, index) => index + 21).map((number) =>
          displaySeats(number)
        )}
      </Grid>
      {/* Add Modal */}
      <AddEditModal
        isEdit={false}
        ticket={{}}
        open={open}
        handleClickOpen={handleClickOpen}
        handleAddEditTicket={handleAddTicket}
      />
    </>
  );
};

export default BusLayout;
