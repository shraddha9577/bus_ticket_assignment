// @imports
import { Typography } from '@mui/material'
import React from 'react'
// @components
import BusLayout from "../busLayout/BusLayout";

const Dashboard = () => {
  return (
    <div className='container'>
        <Typography
        variant="h4"
        mt={3}
        sx={{ color: "darkmagenta" }}
      >
        Reservation View.
      </Typography>
      <Typography variant="p" mt={3}>
      Click on Available seat to proceed with your transaction.
      </Typography>
      <BusLayout/>
    </div>
  )
}

export default Dashboard
